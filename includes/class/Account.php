<?php

class Account
{
	private $con;
	private $errorArray;
	public function __construct($con)
	{
		$this->con=$con;
		$this->errorArray=array();
	}

//	loginf function
    public function loginFunc($username,$password)
    {
        $encpass=md5($password);
        $loginQ="SELECT * FROM users WHERE username='$username' AND password='$encpass'";
        $thesql=mysqli_query($this->con,$loginQ);
        if (!$thesql)
        {
            echo "sql error";
        }

        if (mysqli_num_rows($thesql)==1)
        {
            return true;
        }
        else
        {
            array_push($this->errorArray,ConstaE::$loginError);
            return false;
        }

    }
	public function  register($username,$firstName,$lastName,$email,$confirmEmail,$password,$confirmPassword)
	{
			//TODO:calling the validation function
        $this->usernamValidation($username);
        $this->firstNameValifdation($firstName);
        $this->lastNameValifdation($lastName);
        $this->passwordValidation($password,$confirmPassword);
        $this->emailValidation($email,$confirmEmail);
        if (empty($this->errorArray))
        {
            return $this->insertDB($username,$firstName,$lastName,$email,$password);
        }
        else
        {
            return false;
        }

	}
//	checking the errorarray is empty or not
    public function getError($msg)
    {
        if (!in_array($msg,$this->errorArray))
        {
            $msg="";
        }
        return "<span class='erroMsg'>$msg</span>";
    }
//    inserting the values in the DB is everything is ok
    private function  insertDB($un,$fn,$ln,$em,$pass)
    {
            $hashedPassword=md5($pass);
            $date=Date("Y-m-d");

            $insert="INSERT INTO users VALUES(NULL,'$un','$fn','$ln','$em','$hashedPassword','$date')";
            $result=mysqli_query($this->con,$insert);
            return $result;

    }
	// validaitng the form data
	private function usernamValidation($username)
	{
		//TODO:do username vlidation
        if (strlen($username)>10||strlen($username)<5)
        {
//            array_push($this->errorArray,"Username must be under between 5-10 charector");
            array_push($this->errorArray,ConstaE::$usernameLength);
            return;
        }

        //TODO:check the username in db
        $username_Query="SELECT username FROM users WHERE username='$username'";
        $sql=mysqli_query($this->con,$username_Query);
        if (!$sql)
        {
            echo "SQL ERROr in username validation";
            return;
        }

        if (mysqli_num_rows($sql)>0)
        {
//            array_push($this->errorArray,"username already taken");
            array_push($this->errorArray,ConstaE::$usernameTaken);
            return;
        }

	}
//	firstname validation
	private  function firstNameValifdation($firstname)
    {
        if (strlen($firstname)>10||strlen($firstname)<=2)
        {
//            array_push($this->errorArray,"Firstname must be in 5-10 charector");
            array_push($this->errorArray,ConstaE::$firstNameLength);
            return;
        }


    }
//    lastname validation
    private  function lastNameValifdation($lastname)
    {
        if (strlen($lastname)>15||strlen($lastname)<=2)
        {
//            array_push($this->errorArray,"lastname must be between 5-15 charector");
            array_push($this->errorArray,ConstaE::$lastnameLength);
            return;
        }
    }
//    password validation
	private  function  passwordValidation($password,$confirmPassword)
    {
        if($password!=$confirmPassword)
        {
//            array_push($this->errorArray,"Password is not matching");
            array_push($this->errorArray,ConstaE::$passwordNotMatching);
            return;
        }
        if (preg_match('/[^A-Za-z0-9]/',$password))
        {
//            array_push($this->errorArray,"Password  contain only numbers and letters");
            array_push($this->errorArray,ConstaE::$passwordInvalid);
            return;
        }
        if (strlen($password)>15||strlen($password)<5)
        {
//            array_push($this->errorArray,"Password is too small");
            array_push($this->errorArray,ConstaE::$passwordSmall);
            return;
        }

    }
//    email validation
    private  function  emailValidation($email,$confirmEmail)
    {
        if ($email!=$confirmEmail)
        {
//            array_push($this->errorArray,"Email is not matching");
            array_push($this->errorArray,ConstaE::$emailNotMatching);
            return;
        }
        if (!filter_var($email,FILTER_VALIDATE_EMAIL))
        {
//            array_push($this->errorArray,"Invalid email");
            array_push($this->errorArray,ConstaE::$emailInvalid);
            return;
        }
        //TODO:check the email in DB
        $email_Query="SELECT email FROM users WHERE email='$email'";
        $Email_sql=mysqli_query($this->con,$email_Query);
        if (!$Email_sql)
        {
            echo "SQL ERROR IN EMAIL VALIDATION FUNCTION";
            return;
        }
        if (mysqli_num_rows($Email_sql)>0)
        {
//            array_push($this->errorArray,"Email is already taken");
            array_push($this->errorArray,ConstaE::$emailTaken);
            return;
        }


    }

}



 ?>