<?php
class ConstaE
{
//        register Error message
        public static $usernameLength="Username must be under between 5-10 charector";
        public static $usernameTaken="username already taken";

        public static $firstNameLength="Firstname must be in 2-10 charector";

        public static $lastnameLength="lastname must be between 2-15 charector";

        public static $passwordNotMatching="Password is not matching";
        public static $passwordInvalid="Password  contain only numbers and letters";
        public static $passwordSmall="Password is too small";

        public static $emailNotMatching="Email is not matching";
        public static $emailInvalid="Invalid email";
        public static $emailTaken="Email is already taken";

//        login error
        public static $loginError="Invalid username or password";
}
?>