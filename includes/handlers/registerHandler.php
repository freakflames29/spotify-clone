<?php
// username sanitizing
//$connec=$con;
function usernameSanitize($input)
{
//    $input = mysqli_real_escape_string($connec, $input);
    $input = strip_tags($input);
    $input = str_replace(" ", "", $input);
    $input = ucfirst(strtolower($input));
    // echo $input;
    return $input;
}

// password sanitizing
function passwordSantize($input)
{
//    $input = mysqli_real_escape_string($connec, $input);
    $input = strip_tags($input);
    return $input;
}

// string sanitizing
function stringSanitizing($input)
{
//    $input = mysqli_real_escape_string($connec, $input);

    $input = strip_tags($input);
    $input = str_replace(" ", "", $input);
    $input = ucfirst(strtolower($input));
    return $input;

}

function emailSanitize($input)
{
//    $input = mysqli_real_escape_string($connec, $input);
    $input = strip_tags($input);
    $input = str_replace(" ", "", $input);
    return $input;
}

if (isset($_POST['registerButton'])) {
    $username = usernameSanitize($_POST['RegisterUsername']);
    $firstName = stringSanitizing($_POST['FirstName']);
    $lastName = stringSanitizing($_POST['LastName']);
    $email = emailSanitize($_POST['Email']);
    $confirmEmail = emailSanitize($_POST['ConfirmEmail']);
    $password = passwordSantize($_POST['password']);
    $confirmPassword = passwordSantize($_POST['ConfirmPassword']);
    // echo $confirmEmail;

    $wasSucc = $acc->register($username, $firstName, $lastName, $email, $confirmEmail, $password, $confirmPassword);
    if ($wasSucc)
    {
        header("Location:index.php");
    }
//    else
//    {
//        echo "insertion in DB failed check the insertDB function in Account class";
//    }

}

?>