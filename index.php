<?php
include "includes/config.php";

if (isset($_SESSION['userLoggedIn']))
{
    //echo "Hello ".'<strong>'.$_SESSION['userLoggedIn'].'</strong>';
}
else
{
    header("Location:register.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Welcome to spotify</title>
	<link rel="stylesheet" href="includes/style/style.css">
</head>
<body>
	<div id="mainContainer">
		
			<div id="topContainer">
					<div id="navBarContainer">
							<nav class="navbar">
								<a href="#" class="logo">
									<img src="https://storage.googleapis.com/pr-newsroom-wp/1/2018/11/Spotify_Logo_CMYK_Green.png" alt="spotify logo">
								</a>

								<div class="group">
									<div class="navItem">
										<a href="#">
										<span class="navItemLink">Search</span>
										<img src="includes/images/icons/search.png" alt="searcddd" class="searchIcon">
										</a>
									</div>
								</div>

								<div class="group">
									<div class="navItem">
										<a href="#">
										<span class="navItemLink">Browse</span>
										</a>
									</div>
									<div class="navItem">
										<a href="#">
										<span class="navItemLink">Your Music</span>
										</a>
									</div>
									<div class="navItem">
										<a href="#">
										<span class="navItemLink">Your profile</span>
										</a>
									</div>
								</div>
							</nav>					
					</div>
				</div>
				
			</div>

			<div id="nowPlayingBarContainer">
			<div id="nowPlayingBar">
				<div id="nowPlayingBarLeft">
					<div class="content">
						<span class="albumLink">
							<img src="https://d138zd1ktt9iqe.cloudfront.net/media/seo_landing_files/area-of-square-01-1597849417.png" alt="Album image" class="artwork">
						</span>
						<div class="trackinfo">
							<div class="trackname">
								<span>Another day of sun</span>
							</div>
							<div class="artistname">
								<span>Justin Hurtwitz</span>
							</div>
						</div>
					</div>
					
				</div>
				<div id="nowPlayingBarCenter">
					<div class="content playerControl">
						<div class="buttons">
							<button class="ControlButton shuffle">
								<img src="includes/images/icons/shuffle.png" alt="Shuffle">
							</button>
							<button class="ControlButton previous">
								<img src="includes/images/icons/previous.png" alt="Shuffle">
							</button>
							<button class="ControlButton play">
								<img src="includes/images/icons/play.png" alt="Shuffle">
							</button>	
							<button class="ControlButton pause" style="display: none;">
								<img src="includes/images/icons/pause.png" alt="Shuffle">
							</button>
							<button class="ControlButton next">
								<img src="includes/images/icons/next.png" alt="Shuffle">
							</button>
							<button class="ControlButton repeat">
								<img src="includes/images/icons/repeat.png" alt="Shuffle">
							</button>


						</div>
						<div class="playbackBar">
							<span class="progressTime current">0.00</span>
							<div class="progressBar">
								<div class="progressBarbg">
									<div class="progress"></div>
								</div>
							</div>
							<span class="progressTime remaining">4.02</span>
						</div>
					</div>
					
				</div>
				<div id="nowPlaingBarRight">
					<div class="volumeBar">
						<div class="buttons">
							<button class="volume ControlButton">
								<img src="includes/images/icons/volume.png" alt="vloumebtn">
							</button>
						</div>
						<div class="progressBar">
								<div class="progressBarbg">
									<div class="progress"></div>
								</div>
						</div>

					</div>
				</div>
			</div>
			
		</div>

	</div>
	
	
</body>
</html>