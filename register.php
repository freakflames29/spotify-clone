<?php
include 'includes/config.php';
include 'includes/class/ConstaE.php';
include 'includes/class/Account.php';
$acc=new Account($con);
include 'includes/handlers/login-handlers.php';
include 'includes/handlers/registerHandler.php';
function getValue($name)
{
    if (isset($_POST[$name]))
    {
        echo $_POST[$name];
    }
}
if (isset($_SESSION['userLoggedIn']))
{
    header("Location:index.php");
}
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Regitster</title>
    <link rel="stylesheet" href="includes/style/register.css">
    <link rel="stylesheet" href="includes/style/icofont/icofont.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

</head>
<body>
    <div id="background">
        <div id="loginContainer">
            <div id="inputContainer">
                    <!-- login form-->
                <form action="register.php" method="post" id="loginForm">
                    <h2>Login to your account</h2>
                    <p>
                        <?php echo $acc->getError(ConstaE::$loginError)?>
                        <label for="LoginUsername">Username</label>
                        <input type="text" placeholder="Username" name="LoginUsername" id="LoginUsername" value="<?php getValue('LoginUsername')?>" required>
                    </p>
                    <p>

                        <label for="LoginPassword">Password</label>
                        <input type="password" placeholder="Password" id="LoginPassword" name="LoginPassword" required>
                    </p>
                        <button name="LoginButton" type="submit">Login</button>

                    <div class="hasAccount">
                        <span id="hidelogin">Don't have an account? signup here</span>
                    </div>
                    
                </form>
<!--                register form-->
                <form action="register.php" method="post" id="RegisterForm">
                    <h2>Register Now for free</h2>
                    <p>
                        <?php echo $acc->getError(ConstaE::$usernameLength)?>
                        <?php echo $acc->getError(ConstaE::$usernameTaken)?>
                        <label for="RegisterUsername">Username</label>
                        <input type="text" placeholder="Username" name="    RegisterUsername"  id="RegisterUsername" value="<?php getValue('RegisterUsername');?>" required>
                    </p>
                    <p>
                        <?php echo $acc->getError(ConstaE::$firstNameLength)?>
                        <label for="FirstName ">First Name</label>
                        <input type="text" placeholder="First Name" id="FirstName " name="FirstName" value="<?php getValue('FirstName');?>" required>

                    </p>
                    <p>
                        <?php echo $acc->getError(ConstaE::$lastnameLength)?>
                        <label for="LastName ">Last Name</label>
                        <input type="text" placeholder="Last Name" id="LastName " name="LastName" value="<?php getValue('LastName');?>" required>

                    </p>
                    <p>
                        <?php echo $acc->getError(ConstaE::$emailNotMatching) ?>
                        <?php echo $acc->getError(ConstaE::$emailInvalid) ?>
                        <?php echo $acc->getError(ConstaE::$emailTaken) ?>
                        <label for="Email">Email </label>
                        <input type="email" placeholder="Email" id="Email" name="Email" value="<?php getValue('Email');?>" required>
                    </p>

                    <p>
                        <label for="ConfirmEmail">Confirm Email </label>
                        <input type="email" placeholder="Confirm Email" id="ConfirmEmail" name="ConfirmEmail" value="<?php getValue('ConfirmEmail');?>" required>
                    </p>

                    <p>
                        <?php echo $acc->getError(ConstaE::$passwordNotMatching)?>
                        <?php echo $acc->getError(ConstaE::$passwordInvalid)?>
                        <?php echo $acc->getError(ConstaE::$passwordSmall)?>
                        <label for="password"> Password</label>
                        <input type="password" placeholder="Password" name="password" id="password">
                    </p>
                    <p>
                        <label for="ConfirmPassword">Confirm Password</label>
                        <input type="password" placeholder="Confirm Password" id="ConfirmPassword" name="ConfirmPassword">
                    </p>
                    <button type="submit" id="registerButton" name="registerButton">Sign Up</button>
                    <div class="hasAccount">
                        <span id="hideregiter">Already have an account? Login here</span>
                    </div>
                </form>

            </div>
            <div id="loginText">
                <h1>Get Great music right now</h1>
                <h2>Listen lots of songs for free</h2>
                <ul>
                    <ol>Dicover music you fall in love with</ol>
                    <ol>Create your own playlist</ol>
                    <ol>Follow your artist to keep uptodate</ol>
                </ul>
            </div>
        </div>


    </div>
    <script src="includes/js/regsiter.js"></script>

    <?php

        if (isset($_POST['registerButton']))
        {
            echo '   <script>
                $("#loginForm").hide();
                $("#RegisterForm").show();
            </script>';
        }
        else
        {
            echo '   <script>
                $("#loginForm").show();
                $("#RegisterForm").hide();
            </script>';
        }


    ?>
</body>
</html>
